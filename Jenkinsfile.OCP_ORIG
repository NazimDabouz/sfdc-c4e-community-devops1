@Library('aes@3.6.0') _

def applicationList = []
def currentReleasedApplications = []

def release=null
def gitCommitHash=null

def tfs = [:]
def jenkinsGit='JenkinsGit'

def releaseApprover = ""

def getFirstCommit() {
  sh(returnStdout: true, script: 'git log -1  --pretty="%H"').trim()
}

def getPreviousMicroserviceInfo(currentReleasedApplications, microservice) {
	for(current in currentReleasedApplications) {
		if(current.name.equals(microservice)) {
			return current
		}
	}
	return null
}

node('centos-small') {
	dir("release") {
	
		stage("Checkout") {
			gitCommitHash = checkout scm
			echo "gitCommitHash: ${gitCommitHash}"			
		}

		stage("Load settings") {
			def oneclick = readYaml file: "1click.yml"
			
			for(app in oneclick.services) {
				def c = [:]
				c["name"] = app.key
				c["version"] = app.value
				applicationList.add(c)
			}		
			release = oneclick.release[env.JOB_BASE_NAME]
			echo "release = ${release}"

			if(oneclick.git != null) {
				jenkinsGit = oneclick.git.cred
				tfs.host = oneclick.git.host
				tfs.area = oneclick.git.area
				tfs.project = oneclick.git.project
			}
		}
	}
	
	stash allowEmpty: true, useDefaultExcludes: false, name: 'release'
}
	
if(release?.confirm ?: false || (release.serviceNow?.enabled ?: false)) {
	stage("Confirmation") {
		timeout(time: 5, unit: 'DAYS') {
			releaseApprover = input message: 'Do you want to release this build?', submitterParameter: 'releaseApprover'
		}		
	}
}
		
node('centos-small') {
	unstash 'release'
	
	if((release.serviceNow?.enabled ?: false) && (release.mergeTo != null)) {
		sh 'mkdir -p previous-release/abc && rm -rf previous-release'	
		dir("previous-release") {
			stage("Checkout previous released version") {
				def git = new com.bp.pipeline.git()
				git.branchlessCheckout(gitCommitHash.GIT_URL, "JenkinsGit", release.mergeTo)
			}
			
			stage("Load previous application versions") {
				if(fileExists("1click.yml")) {
					def oneclick = readYaml file: "1click.yml"
					
					for(app in oneclick.services) {
						def c = [:]
						c["name"] = app.key
						c["version"] = app.value
						currentReleasedApplications.add(c)
					}
					echo "currentReleasedApplications: ${currentReleasedApplications}"
				}
			}
		}
	}
	
	if(release.pullImage != null) {
		for(application in applicationList) {
			stage("Pull Image ${application.name}") {
				withCredentials([usernamePassword(credentialsId: release.openshift.credentialId, passwordVariable: 'PASSWORD', usernameVariable: 'USERNAME')]) {
					echo "Pull job: ${release.pullImage.job} - ${release.pullImage.sourceNamespace}/${application.name}, version: ${application.version}"
					build job: "${release.pullImage.job}", wait: true,  parameters: [string( name: 'source_image', value: "${release.pullImage.sourceNamespace}/${application.name}"),string( name: 'target_image', value: "${release.pullImage.targetNamespace}/${application.name}"),string(name: 'image_version', value: application.version),password( name: 'openshift_id', value: PASSWORD)]
				}

			}
		}
	}
	
	dir("apps") {
		for(app in applicationList) {
			dir("${app.name}") {
				stage("Checkout ${app.name}") {
					if (tfs.area == null ) {
						tfs.area = ""
					}
					def oneclick = readYaml file: "../../release/1click.yml"
					checkout ( [$class: 'GitSCM',
						branches: [[name: "${app.version}" ]],
						userRemoteConfigs: [[
							//credentialsId: jenkinsGit,
							credentialsId: oneclick.git.cred, 
							url: "${tfs.host}/${tfs.area}/${tfs.project}/_git/${app.name}"]]])

				}
				if(fileExists("deploy.groovy")) {
					def ticketId = null
					if(release.serviceNow?.enabled ?: false) {
						stage("Creating CR") {
							def currentRelease = getPreviousMicroserviceInfo(currentReleasedApplications, app.name)
							
							echo "${currentRelease} upgrading to ${app}"
							
							if(currentRelease == null) {
								currentRelease = [:]
								currentRelease.version = getFirstCommit()
							}

							def data = tfsGetAuditData {
								url = "${tfs.host}"
								area = "${tfs.area}"
								project = "${tfs.project}"
								repo = "${app.name}"
								startRef = "${currentRelease.version}"
								endRef = "${app.version}"
								credentials = "${jenkinsGit}"
							}
							
							def reviewers = ""
							
							for(pr in data) {
								for(reviewer in pr.reviewers) {
									if(reviewers != "") {
										reviewers += ", "
									}
									reviewers += reviewer
								}
							}
							
							echo "${data}"
							
							ticketId = snowCreateChange {
								url = "https://bpgamma.service-now.com"
								id = release.serviceNow.ciId
								auditData = data.toString()
								credentials = release.serviceNow.credentials
								prApprovers = reviewers
								scmTag = "${app.version}"
								duration = release.serviceNow.duration
								microservice = "${app.name}"
								microserviceVersion = "${release.pullNamespace}/${app.name}:${app.version}"
								approver = releaseApprover
								platform = release.serviceNow.platform
							}
							echo "Service Now Ticket ID: ${ticketId}"
							if(ticketId == null) {
								echo 'No Service Now ticket returned'
								currentBuild.result = 'FAILURE'
								return
							}
							snowStartChange {
								url = "https://bpgamma.service-now.com"
								id = ticketId
								credentials = release.serviceNow.credentials
							}
						}
					}

					def status = false
					try {
						stage("Deploy ${app.name}") {
							def deploy = load("deploy.groovy")
							deploy.deploy(release, app)
							status = true
						}
					} finally {
						if(release.serviceNow?.enabled ?: false) {				
							stage("Closing CR") {
								snowCompleteChange {
									url = "https://bpgamma.service-now.com"
									id = ticketId
									credentials = release.serviceNow.credentials
									statusSuccessful = status
								}
							}
						}
					}
				}
			}
		}
		
	}
	
	if(release.mergeTo != null) {
		stage("Merge") {
			dir("release") {
				def oneclick = readYaml file: "1click.yml"
				gitConfigure {
					credentials = oneclick.git.cred
					dns = oneclick.git.host.split("/")[2].split(":")[0]
				} 
			
				sh "(git branch -r --contains ${release.mergeTo} && git checkout ${release.mergeTo}) || true"
				sh "(git branch -r --contains ${release.mergeTo} || git checkout -b ${release.mergeTo})"
				sh "git merge ${gitCommitHash.GIT_COMMIT}"
				sh "git push origin ${release.mergeTo}"
			}
		}
	}

	if(release.tag != null) {
		stage("Tag") {
			dir("release") {
				gitConfigure {
					credentials = oneclick.git.cred 
					dns = oneclick.git.host.split("/")[2].split(":")[0]
				}
			
				sh "git tag \"${release.tag}-${new Date().format( 'yyyyMMddHHmmss' )}\""
				sh "git push --tags"
			}
		}
	}
}
