#!/bin/bash

#####################################################
## Name : check_file_name_standards.sh
##
## Purpose : This script checks the file names of the
##           SalesForce metadata files against the
##           defined declarative standards
##
## Author : Venkata Hareesh Sanagavarapu
##
## High Level Logic: Receive the directory to be
##                   scanned recursively and check
##                   each file through defined stan-
##                   dard naming conventions.
####################################################

# Receive the directory to be scanned into a variable

dir_to_scan="$1"


# Check if the directory exists and is actually a directory

if [ ! -d "$dir_to_scan" ]; then
	echo "ERROR : The directory $dir_to_scan is not a valid directory. Please check the directory and try again"
	exit
fi

#Creating XML Header

xmlheader='<?xml version="1.0" encoding="UTF-8"?><DeclarativeStandardsViolators>'

xmlfooter="</DeclarativeStandardsViolators>"

resultsFile="$dir_to_scan/salesforce_coding_declarative_standard_violators.scanResultsXml"

echo  '<?xml version="1.0" encoding="UTF-8"?>' > "$resultsFile"
printf "<DeclarativeStandardsViolators>\n" >> "$resultsFile"


# If the directory is a valid directory to be scanned, proceed with the Salesforce Metadata file naming convention checks

#find "$dir_to_scan" -type f -exec echo "{}" \;
find "$dir_to_scan" -type f -exec bash -c '
	resultsFile="$1/salesforce_coding_declarative_standard_violators.scanResultsXml"
	for file do
		full_path_of_file=$file
		just_file_name=$(basename "$file")

	######## Working on the .object file naming standards

		if [[ "$just_file_name" = *".object" ]]; then
			if [[ "$just_file_name" = *"__c"* ]]; then
				#echo "Detected custom object file $just_file_name"
				if ! [[ "$just_file_name" =~ ^[A-Z0-9]{3}_ ]]; then
					echo "File naming standard for $just_file_name is NOT correct"
					msg="\t<Object>"$just_file_name"</Object>\n"
					printf "$msg" >> "$resultsFile"
				fi			
			fi
		fi

	######### Working on the custom Applications .app file naming standards

		if [[ "$just_file_name" = *".app" ]]; then
			if [[ "$just_file_name" != "standard__"* ]]; then
				if ! [[ "$just_file_name" =~ ^[A-Z0-9]{3}_ ]]; then
					echo "File naming standard for $just_file_name is NOT correct"
					msg="\t<App>$just_file_name</App>\n"
					printf "$msg" >> "$resultsFile"
				fi			
			fi
		fi

	######### Working on the Lightening components (.cmp) file naming standards

		if [[ "$just_file_name" = *".cmp" ]]; then
			if ! [[ "$just_file_name" =~ ^[A-Z0-9]{3}_ ]]; then
				echo "File naming standard for $just_file_name is NOT correct"
				msg="\t<Lightening>$just_file_name</Lightening>\n"
				printf "$msg" >> "$resultsFile"
			fi			
		fi

	######### Working on the Classes (.cls) file naming standards

		if [[ "$just_file_name" = *".cls" ]]; then
			if ! [[ "$just_file_name" =~ ^[A-Z0-9]{3}_ ]]; then
				echo "File naming standard for $just_file_name is NOT correct"
				msg="\t<Class>$just_file_name</Class>\n"
				printf "$msg" >> "$resultsFile"
			fi			
		fi

	######### Working on the CustomMetadata (.md) file naming standards

		if [[ "$just_file_name" = *".md" ]] && [[ "$just_file_name" != "README.md" ]]; then
			if ! [[ "$just_file_name" =~ ^[A-Z0-9]{3}_ ]]; then
				echo "File naming standard for $just_file_name is NOT correct"
				msg="\t<CustomMetadata>$just_file_name</CustomMetadata>\n"
				printf "$msg" >> "$resultsFile"
			fi			
		fi

	######### Working on the Custom Permissions (.customPermission) file naming standards

		if [[ "$just_file_name" = *".customPermission" ]]; then
			if ! [[ "$just_file_name" =~ ^[A-Z0-9]{3}_ ]]; then
				echo "File naming standard for $just_file_name is NOT correct"
				msg="\t<CustomPermission>$just_file_name</CustomPermission>\n"
				printf "$msg" >> "$resultsFile"
			fi			
		fi

	######### Working on the Flexipages (.flexipage) file naming standards

		if [[ "$just_file_name" = *".flexipage" ]]; then
			if ! [[ "$just_file_name" =~ ^[A-Z0-9]{3}_ ]]; then
				echo "File naming standard for $just_file_name is NOT correct"
				msg="\t<FlexiPage>$just_file_name</FlexiPage>\n"
				printf "$msg" >> "$resultsFile"
			fi			
		fi

	######### Working on the Flows (.flow) file naming standards

		if [[ "$just_file_name" = *".flow" ]]; then
			if ! [[ "$just_file_name" =~ ^[A-Z0-9]{3}_ ]]; then
				echo "File naming standard for $just_file_name is NOT correct"
				msg="\t<Flow>$just_file_name</Flow>\n"
				printf "$msg" >> "$resultsFile"
			fi			
		fi
		
	######### Working on the Global Value Sets (.globalValueSet) file naming standards

		if [[ "$just_file_name" = *".globalValueSet" ]]; then
			if ! [[ "$just_file_name" =~ ^[A-Z0-9]{3}_ ]]; then
				echo "File naming standard for $just_file_name is NOT correct"
				msg="\t<GlobalValueSet>$just_file_name</GlobalValueSet>\n"
				printf "$msg" >> "$resultsFile"
			fi			
		fi

	######### Working on the Layouts (.layout) file naming standards

		if [[ "$just_file_name" = *".layout" ]]; then
			if [[ "$just_file_name" != *"__c"* ]]; then
				if ! [[ "$just_file_name" =~ [a-zA-Z0-9_]+-[A-Z][A-Z][A-Z][[:space:]_].*layout ]]; then
					echo "File naming standard for $just_file_name is NOT correct"
					msg="\t<Layout>$just_file_name</Layout>\n"
					printf "$msg" >> "$resultsFile"
				fi			
			fi
		fi


	######### Working on the Pages file naming standards

		if [[ "$just_file_name" = *".page" ]]; then
			if ! [[ "$just_file_name" =~ ^[A-Z0-9]{3}_ ]]; then
				echo "File naming standard for $just_file_name is NOT correct"
				msg="\t<ApexPage>$just_file_name</ApexPage>\n"
				printf "$msg" >> "$resultsFile"
			fi			
		fi

	######### Working on the Permission Sets (.permissionset) file naming standards

		if [[ "$just_file_name" = *".permissionset" ]]; then
			if ! [[ "$just_file_name" =~ ^[A-Z0-9]{3}_ ]]; then
				echo "File naming standard for $just_file_name is NOT correct"
				msg="\t<PermissionSet>$just_file_name</PermissionSet>\n"
				printf "$msg" >> "$resultsFile"
			fi			
		fi

	######### Working on the Queues (.queue) file naming standards

		if [[ "$just_file_name" = *".queue" ]]; then
			if ! [[ "$just_file_name" =~ ^[A-Z0-9]{3}_ ]]; then
				echo "File naming standard for $just_file_name is NOT correct"
				msg="\t<Queue>$just_file_name</Queue>\n"
				printf "$msg" >> "$resultsFile"
			fi			
		fi

	######### Working on the Report Types (.reportType) file naming standards

		if [[ "$just_file_name" = *".reportType" ]]; then
			if ! [[ "$just_file_name" =~ ^[A-Z0-9]{3}_ ]]; then
				echo "File naming standard for $just_file_name is NOT correct"
				msg="\t<ReportType>$just_file_name</ReportType>\n"
				printf "$msg" >> "$resultsFile"
			fi			
		fi

	######## Working on the Tabs (.tab) file naming standards

		if [[ "$just_file_name" = *".tab" ]]; then
			if [[ "$just_file_name" = *"__c"* ]]; then
				if ! [[ "$just_file_name" =~ ^[A-Z0-9]{3}_ ]]; then
					echo "File naming standard for $just_file_name is NOT correct"
					msg="\t<Tab>$just_file_name</Tab>\n"
					printf "$msg" >> "$resultsFile"
				fi			
			fi
		fi

	######## Working on the Tabs (.trigger) file naming standards

		if [[ "$just_file_name" = *".trigger" ]]; then
			if ! [[ "$just_file_name" =~ ^[A-Z0-9]{3}_ ]]; then
				echo "File naming standard for $just_file_name is NOT correct"
				msg="\t<Trigger>$just_file_name</Trigger>\n"
				printf "$msg" >> "$resultsFile"
			fi			
		fi
	# printf "$full_path_of_file \n $just_file_name\n"
	done
' sh "$dir_to_scan" {}  +

printf "$xmlfooter" >> "$resultsFile"
