#!/bin/bash -v
#This is the Salesforce Script designed to run as a Parametrised Build Job from Jenkins CI

set -o errexit
set -o xtrace
set -o nounset

# Print debugging info
echo "Printing current slave env for debugging purposes"
printenv
echo "Printing whoami"
whoami

echo WORKSPACE = $WORKSPACE

setEnvironment()
{
  echo "##########################################################################"
  echo "Setting Environment Properties"
  #The environment is determined by the user, these credentials are stored in the Jenkins Credential Store
  export SF_USERNAME=$SFDC_USER
  export SF_PASSWORD=$SFDC_PASSWORD
  #The URL is generally https://test.salesforce.com in all environments except production where it's https://login.salesforce.com
  export SF_SERVER_URL=$SFDC_URL
  #If the checkonly parameter is set to true it will not complete the deployment, it will only check the deployment is possible
  export SF_CHECKONLY=$SFDC_CHECKONLY
  #Depending on the environment the Apex Unit Tests are run or not: NoTestRun, RunLocalTests
  export SF_TESTLEVEL=$SFDC_TESTLEVEL
  echo "##########################################################################"
}


cleanSalesforce(){
    echo "##########################################################################"
    echo "cleanSalesforce and set up the git config"
    git config --global user.email "jenkins@example.com"
    git config --global user.name "jenkins"
    chmod 777 ${WORKSPACE}/ci/cleanupBuild/cleanBuildFile.class
    cp ${WORKSPACE}/ci/cleanupBuild/cleanBuildFile.class cleanBuildFile.class
    echo "Current dir = $(pwd)"
    ls -l 
    java cleanBuildFile
    mv ${WORKSPACE}/package/package.xml ${WORKSPACE}/src/package.xml
    echo "##########################################################################"

}

buildSalesforce(){
    echo "##########################################################################"
    echo "buildSalesforce task to push code to sf . com"
    #ant deployUnpackaged -lib /usr/share/ant/lib/ant-deploy-with-xml-report-task-1.4.jar \
    #ant deployUnpackaged -lib /usr/share/ant/lib/ant-salesforce-37.jar \
    ant deployUnpackaged -lib ${WORKSPACE}/ci/ant-salesforce.jar \
            -propertyfile ${WORKSPACE}/ci/build.properties \
            -Dsf.checkOnly=${SF_CHECKONLY} \
            -Dsf.testLevel=${SF_TESTLEVEL} \
            -Dsf.logLevel=None \
            -Dsf.junitreportdir=reports \
            -Dsf.username=${SF_USERNAME} \
            -Dsf.password=${SF_PASSWORD} \
            -Dsf.serverurl=${SF_SERVER_URL} \
            -f ${WORKSPACE}/ci/build.xml
    echo "##########################################################################"

}

# THIS IS THE ORDER OF EXECUTION
# 1. Set the env variables
setEnvironment

# 3. run the clean step
cleanSalesforce

# 4. run the build
buildSalesforce
