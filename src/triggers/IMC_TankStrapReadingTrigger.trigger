trigger IMC_TankStrapReadingTrigger on IMC_Chemical_Tank_Readings__c (before insert, before update) {
 if(Trigger.isBefore)
    {
      if(Trigger.isInsert)
      {
        IMC_TankStrapReadingTriggerHandler.beforeInsertTankRead(Trigger.new);
      }
      
      if(Trigger.isUpdate)
      {
        IMC_TankStrapReadingTriggerHandler.beforeUpdateTankRead(Trigger.OldMap, Trigger.new);
      }
    }

}