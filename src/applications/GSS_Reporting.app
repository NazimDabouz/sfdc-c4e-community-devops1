<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>View</actionName>
        <comment>Action override created by Lightning App Builder during activation.</comment>
        <content>GSS_Account_Page</content>
        <formFactor>Large</formFactor>
        <skipRecordTypeSelect>false</skipRecordTypeSelect>
        <type>Flexipage</type>
        <pageOrSobjectType>Account</pageOrSobjectType>
    </actionOverrides>
    <brand>
        <headerColor>#0070D2</headerColor>
    </brand>
    <description>Global Subsea Reporting Tool</description>
    <formFactors>Large</formFactors>
    <label>GSS Reporting</label>
    <navType>Standard</navType>
    <tab>GSS_Home</tab>
    <tab>standard-Feed</tab>
    <tab>GSS_Project__c</tab>
    <tab>standard-Account</tab>
    <tab>GSS_Equipment__c</tab>
    <tab>GSS_Summary__c</tab>
    <tab>GSS_Issue__c</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <uiType>Lightning</uiType>
    <utilityBar>GSS_Reporting_UtilityBar</utilityBar>
</CustomApplication>
