/**
* @author Abhishek Bisen
* @date 02/06/2018
*
* @group Chemical Tank
*
* @description Daily Schedule the csv export and send over email the tank readings last 24 data, 
*  Mark them for purge, delete all marked purged records created before 48 hrs
*/

global class IMC_TankReadingExportPurgeApexScheduler Implements Schedulable
{
    private static final string orgWideEmailSender = 'No Reply';
    private static final string reportTimeZone = 'US/Alaska';

    global void execute(SchedulableContext sc) {
        //Fetch tank readings last 24 hrs data, create csv of it, and send it over to email to receipent/cc which are configured/filled in custom settings.
        
        List<IMC_Chemical_Tank_Readings__c> chemicalTanks= [SELECT 
                                                                Name,
                                                                IMC_Barcode__c, 
                                                                IMC_Tank_ID__c,
                                                                IMC_Reading_Level__c, 
                                                                IMC_Reading_Unit__c, 
                                                                createdDate, 
                                                                createdBy.firstname, 
                                                                createdBy.lastname 
                                                            FROM IMC_Chemical_Tank_Readings__c 
                                                            WHERE isDeleted=FALSE AND IMC_IsPurge__c= FALSE LIMIT 9999]; 
              
        Messaging.SendEmailResult [] resultMail = exportCSVAndSendEmail(chemicalTanks);
        
        //Mark all the above extrated tank readings data to be isPurge , they will be deleted post 48 hrs through method purgerlastDayTankReadings.        
        if(resultMail != null && resultMail.size()>0 && resultMail[0].isSuccess())
            markPurgeTankReadings(chemicalTanks);
        
        //Delete isPurge marked tank readings records for last 48 hrs
        purgeLastTwoDaysTankReadings();
    }
    
    private Messaging.SendEmailResult[] exportCSVAndSendEmail(List<IMC_Chemical_Tank_Readings__c> chemicalTanks)
    {
        //This is csv file header/column names
        string csvHeader = 'Tank ID , Barcode, Reading Date, Fluid Reading \n';
        string tanReadingsRows = csvHeader ;
        
        //Create csv data/rows for chemical tank reading records
        for(IMC_Chemical_Tank_Readings__c tank: chemicalTanks)
        {
            string formattedCreatedDate = tank.createdDate.format('yyyy-MM-dd HH:mm:ss', reportTimeZone);
            
            string recordString = tank.IMC_Tank_ID__c + ',' +
                                  tank.IMC_Barcode__c + ',' +
                                  formattedCreatedDate + ',' +                                  
                                  tank.IMC_Reading_Level__c +'\n';
            
            tanReadingsRows = tanReadingsRows +recordString;
        }
        
        //Setup email/attachment for sending csv export/file over email address as fetched from custom setting
        Messaging.EmailFileAttachment csvAttachment = new Messaging.EmailFileAttachment();
        blob csvBlob = Blob.valueOf(tanReadingsRows);
        //Create csv file name using system date and given format
        string csvName= DateTime.newInstance(System.today().year(), System.today().month(), System.today().day()).format('YYYYMM') +System.today().day()+ 'SFTS.csv';
        csvAttachment.setFileName(csvName);
        csvAttachment.setBody(csvBlob);
        
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
     
        String subject = 'Chemical Tank Readings CSV - Alert @ '+ Date.today();
        
        OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where DisplayName = :orgWideEmailSender];
        if (owea.size() > 0) {
            email.setOrgWideEmailAddressId(owea.get(0).Id);
        }
        
        email.setSubject(subject);      
        
        //fetch custom setting values for cc and to email list 
        IMC_Chemical_Tank_CSV_Export__c csvExportSetting = IMC_Chemical_Tank_CSV_Export__c.getAll().get('Tank Readings Export'); 
        List<String> mailToAddresses = new List<String>();  
        List<String> mailCCAddresses = new List<String>();  
        
        // Retrive TO/CC email addresss, body content from custom setting. If custom settings is empty/null , get emails from user groups 
        if(csvExportSetting != null) {
            if(csvExportSetting.IMC_Recipient_Emails__c != null)
                mailToAddresses = csvExportSetting.IMC_Recipient_Emails__c.split(',');
           
            if(csvExportSetting.IMC_CC_Emails__c != null)
                mailCCAddresses= csvExportSetting.IMC_CC_Emails__c.split(','); 
            
            //Retrive email body content from custom setting, and set it in html format
            if(csvExportSetting.IMC_Email_Body__c != null)
            {
                String htmlbody = '<html><body><p>Dear All,</p><p>'+ csvExportSetting.IMC_Email_Body__c +'</p></body></html>';
                email.setHtmlBody(htmlbody); 
            }
        } else {   
            //Get public group user's email addresses
            mailToAddresses = getGroupUsersEmailAddresses();
            mailCCAddresses = getGroupUsersEmailAddresses();            
                   
            email.setPlainTextBody('Dear All, \n Attached is Chemical tank Readings CSV as of today '+Date.today() + '\n'); 
        }
        
        //Set TO/CC email addressess retrived either from custom setting or public group
        if (mailToAddresses != null && !mailToAddresses.isEmpty())
                email.setToAddresses( mailToAddresses );
        if (mailCCAddresses != null && !mailCCAddresses.isEmpty())
                email.setCCAddresses( mailCCAddresses );            
               
        email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttachment});
        Messaging.SendEmailResult [] resultMail;
        
        try {
          resultMail = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
        } catch(EmailException ex) {}
        
        return resultMail;
        
    }
    
    //if CSV export email custom settings is not filled , fetch public group users list and email to them the csv export
    private List<String> getGroupUsersEmailAddresses() {
        List<String> idList = new List<String>();
        List<String> mailToAddresses = new List<String>();
        
        //fetch pre-defined public group
        List<Group> groupList = [SELECT (SELECT userOrGroupId FROM groupMembers) 
                                 FROM group 
                                 WHERE name = 'Chemical Tank Reading Reporting- Admin'];
        
        //Create list of users id which are included in group
        for (Group groupNew : groupList) {
            for (GroupMember groupMember : groupNew.groupMembers) {
                idList.add(groupMember.userOrGroupId);
            }
        }
        
        User[] usrList = [SELECT email FROM user WHERE id IN :idList];
        //Add group users email address
        for(User userNew : usrList) {
         mailToAddresses.add(userNew.email);
        }
        return mailToAddresses;
    }
    
    //mark all tank readings ready for purge in next job run after 48 hrs for recently exported and sent over email
    private void markPurgeTankReadings(List<IMC_Chemical_Tank_Readings__c> lastDayTankReadings)
    {
        for(IMC_Chemical_Tank_Readings__c tankReading: lastDayTankReadings) {
            tankReading.IMC_IsPurge__c = true;
        }
        
        try {
            if(!lastDayTankReadings.isEmpty())
                database.update(lastDayTankReadings, true);
        } catch(Exception e){}
    }
    
    // Delete all isPurge marked tank readings which were created last 48 hrs and ther are not yet deleted
    private void purgeLastTwoDaysTankReadings()
    {
        IMC_Chemical_Tank_CSV_Export__c csvExportSetting = IMC_Chemical_Tank_CSV_Export__c.getAll().get('Tank Readings Export');
        if(csvExportSetting != null && csvExportSetting.IMC_IsPurgeRequired__c) {
            try {
                List<IMC_Chemical_Tank_Readings__c> chemicalTanks= [SELECT Id 
                                                                FROM IMC_Chemical_Tank_Readings__c 
                                                                WHERE createddate <: SYSTEM.now()-csvExportSetting.IMC_Readings_Purge_Days__c 
                                                                AND IMC_IsPurge__c = TRUE 
                                                                AND isDeleted=FALSE];
           
                if(!chemicalTanks.isEmpty())
                    database.delete(chemicalTanks, true);
            } catch(Exception e) {}
        }
    }

}