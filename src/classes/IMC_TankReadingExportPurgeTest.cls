/**
* @author Abhishek Bisen
* @date 02/06/2018
*
* @group Chemical Tank
*
* @description Test Class for IMC_TankReadingExportPurgeApexScheduler
*/

@isTest
private class IMC_TankReadingExportPurgeTest {
   //Create tank info master records, and some tank readings records without any lookup to tank info object
    static void createTankInfoReadings()
    {
        IMC_tank_Info__c mtr = new IMC_tank_Info__c();
        mtr.IMC_Tank_ID__c= '16-1900-10XX';
        mtr.IMC_Barcode__c ='CIC085694';
        mtr.IMC_Chemical_ID__c='NALCO EC1509A';
        mtr.IMC_Facility_Location__c='FS2';
        mtr.IMC_Module_Number__c='4906';
        mtr.IMC_SG_Units__c='INCHES';  
        insert mtr;
         
        IMC_Chemical_Tank_Readings__c tsr = new IMC_Chemical_Tank_Readings__c();
        tsr.IMC_Barcode__c='CIC085694';
        tsr.IMC_Reading_Level__c=111;
		insert tsr;  
         
        IMC_Chemical_Tank_Readings__c tsr1 = new IMC_Chemical_Tank_Readings__c();
        tsr1.IMC_Barcode__c='CIC085694';
        tsr1.IMC_Reading_Level__c=222;
		insert tsr1; 
        
    }
    //Execute csv export method when custom settings is filled
    @isTest
     static void executeTestWithCustomSetting(){ 
       
        createTankInfoReadings();
        test.startTest();
           //Create a custom setting record for csv export attributes 
           insert new IMC_Chemical_Tank_CSV_Export__c(Name = 'Tank Readings Export', 
                                                       IMC_Recipient_Emails__c = 'abhibise@in.ibm.com', 
                                                       IMC_CC_Emails__c = 'abhibise@in.ibm.com', 
                                                       IMC_Email_Body__c = 'PFA csv attachment', 
                                                       IMC_Readings_Purge_Days__c = 0,
                                                       IMC_IsPurgeRequired__c = true);
         
            IMC_TankReadingExportPurgeApexScheduler tankReadingApexScheduler = new IMC_TankReadingExportPurgeApexScheduler();
			tankReadingApexScheduler.execute(null);      
        test.stopTest();
         
        List<IMC_Chemical_Tank_Readings__c> tankReadingsList = [SELECT 
                                                                    Name, 
                                                                    IMC_Barcode__c, 
                                                                    IMC_Tank_ID__c,
                                                                    IMC_Reading_Level__c, 
                                                                    IMC_Reading_Unit__c, 
                                                                    createdDate, 
                                                                    createdBy.firstname, 
                                                                    createdBy.lastname 
                                                                FROM IMC_Chemical_Tank_Readings__c];
                
        for(IMC_Chemical_Tank_Readings__c readingsNew : tankReadingsList)
         {
             System.assert(readingsNew.IMC_IsPurge__c = true, 'Expect tank readings record has been updated for isPurge true(ready for delete) after export scv over email');
         }            

     }
     //Execute csv export method when custom settings is empty, it will get to/cc emails from user group
    @isTest
     static void executeTestWithoutCustomSetting(){
       
        createTankInfoReadings();
        test.startTest();
             IMC_TankReadingExportPurgeApexScheduler tankReadingApexScheduler = new IMC_TankReadingExportPurgeApexScheduler(); 
			 tankReadingApexScheduler.execute(null);      
        test.stopTest();  
         
        List<IMC_Chemical_Tank_Readings__c> tankReadingsList = [SELECT 
                                                                    Name,
                                                                    IMC_Barcode__c, 
                                                                    IMC_Tank_ID__c,
                                                                    IMC_Reading_Level__c, 
                                                                    IMC_Reading_Unit__c, 
                                                                    createdDate, 
                                                                    createdBy.firstname, 
                                                                    createdBy.lastname 
                                                                FROM IMC_Chemical_Tank_Readings__c];
        
        for(IMC_Chemical_Tank_Readings__c readingsNew : tankReadingsList) 
        {
            System.assert(readingsNew.IMC_IsPurge__c = true, 'Expect tank readings record has been updated for isPurge true(ready for delete) after export scv over email');
        }      
     }
}