/**
* @author Abhishek Bisen
* @date 02/19/2018
*
* @group Chemical Tank
*
* @description Auto-Populate Tank Info lookup field in Tank Readings record from Tank Info object for the entered barcode 
*/

public with sharing class IMC_TankStrapReadingTriggerHandler
{
    /**
	* @description : Auto-Populate Tank Info lookup field in Tank Readings record from Tank Info object for the entered barcode
	* @param : List<IMC_Chemical_Tank_Readings__c>
	* @return : Void
	*/

    public static void beforeInsertTankRead(List<IMC_Chemical_Tank_Readings__c> tsrNewList)
    {
         Set<String> barcodeSet= new Set<String>();   
        
        /*Create set of all barcode values for insrted tank readings, to handle bulk insert
         * Barcode may be entred in any case by user, so handled it by using a formula field in Tank Info object which is used in SOQL where clause
         * 
         */ 
        for(IMC_Chemical_Tank_Readings__c tNew : tsrNewList)
        {
            if(tNew.IMC_Barcode__c != null)
             barcodeSet.add(tNew.IMC_Barcode__c.touppercase());
        }  
        
        if(barcodeSet!= null & !barcodeSet.isEmpty())
            populateTankInfoLookup(barcodeSet, tsrNewList);
    }
    
    /**
	* @description : Auto-Populate/update Tank Info lookup field in Tank Readings record from Tank Info object for the updated barcode
	* @param : Map<Id, IMC_Chemical_Tank_Readings__c>, List<IMC_Chemical_Tank_Readings__c>
	* @return : Void
	*/

    public static void beforeUpdateTankRead(Map<Id, IMC_Chemical_Tank_Readings__c> oldMap, List<IMC_Chemical_Tank_Readings__c> tsrNewList)
    {
        Set<String> barcodeSet= new Set<String>();   
        
        /*Create set of all barcode values for only updated tank readings/barcode value, to handle bulk update
         * Barcode may be entred in any case by user, so handled it by using a formula field in Tank Info object which is used in SOQL where clause
         * 
         */ 
        for(IMC_Chemical_Tank_Readings__c tNew : tsrNewList)
        {
            if(tNew.IMC_Barcode__c != null && 
               oldMap != null &&
               oldMap.get(tNew.id).IMC_Barcode__c != tNew.IMC_Barcode__c)
             	 barcodeSet.add(tNew.IMC_Barcode__c.touppercase());
        }
        
        //Populate tank info lookup in tank reading records on insert/update
        if(barcodeSet!= null & !barcodeSet.isEmpty())
            populateTankInfoLookup(barcodeSet, tsrNewList);
       
    }
    
    //This method is used for populate tank info lookup in tank reading records on insert/update
    private static void populateTankInfoLookup(Set<String> barcodeSet, List<IMC_Chemical_Tank_Readings__c> tsrNewList)
    {
      try{ 
        //Create map of Tank Info object records for entered barcode values, It will be used to retrive Tank Info object id/lookup to readings record
        List<IMC_Tank_Info__c> tankInfoList = [SELECT IMC_Barcode__c 
                                               FROM IMC_Tank_Info__c 
                                               WHERE IMC_Barcode_UpperCase__c in: barcodeSet];
        Map<String, IMC_Tank_Info__c> tankInfoMap = new Map<String, IMC_Tank_Info__c>();
       
        //Create map of Tank Infos for received barcodes, key vale is barcode which is stored in upper case to match barcode in tank Info to avoid case insensitivity
        for(IMC_Tank_Info__c trmNew : tankInfoList)
        {
            tankInfoMap.put(trmNew.IMC_Barcode__c.touppercase(), trmNew);
        }
      
        for(IMC_Chemical_Tank_Readings__c tsrNew: tsrNewList)  
        {
           if(tsrNew.IMC_Barcode__c != null &&
              tankInfoMap.containsKey(tsrNew.IMC_Barcode__c.touppercase()) ) 
           {  
              //Populate tank info lookup only if barcode value has been changed  
              tsrNew.IMC_Tank_Name__c = tankInfoMap.get(tsrNew.IMC_Barcode__c.touppercase()).id;
              
           }else if(tsrNew.IMC_Barcode__c != null && tankInfoMap.isEmpty())
           {
               tsrNew.IMC_Barcode__c.addError('Barcode ' + tsrNew.IMC_Barcode__c + ' is not found in Tank Info'); // Show error message if entered barcode is not found in tank info master object
           } 
               
        }
      }catch(Exception ex){}
    }
    
}