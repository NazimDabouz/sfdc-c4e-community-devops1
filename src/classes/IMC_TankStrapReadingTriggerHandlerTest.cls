/**
* @author Abhishek Bisen
* @date 01/20/2018
*
* @group Chemical Tank
*
* @description Test Class for IMC_TankStrapReadingTriggerHandler and IMC_TankStrapReadingTrigger.
*/

@isTest
private class IMC_TankStrapReadingTriggerHandlerTest {
    static final integer TANK_READINGS_RECORD_COUNT = 101;
    
    @isTest
    //create tank info parent object records
    static void createTankInfo(){
        IMC_tank_Info__c mtr = new IMC_tank_Info__c();
        mtr.IMC_Tank_ID__c= '16-1900-10XX';
        mtr.IMC_Barcode__c ='CIC085694';
        mtr.IMC_Chemical_ID__c='NALCO EC1509A';
        mtr.IMC_Facility_Location__c='FS2';
        mtr.IMC_Module_Number__c='4906';
        mtr.IMC_SG_Units__c='INCHES';  
        insert mtr;
        
        IMC_tank_Info__c mtr1 = new IMC_tank_Info__c();
        mtr1.IMC_Tank_ID__c= '16-1900';
        mtr1.IMC_Barcode__c ='CIC085695';
        mtr1.IMC_Chemical_ID__c='NALCO EC15010A';
        mtr.IMC_Facility_Location__c='FS1';
        mtr1.IMC_Module_Number__c='4907';
        mtr1.IMC_SG_Units__c='PERCENT';  
        insert mtr1;
    }
    
    //Create tank readings records in bulk for same barcode , it will be used to test bulk insert tank readings records
    static List<IMC_Chemical_Tank_Readings__c> createTankReadingRecords(Integer recNo)
    {
        List<IMC_Chemical_Tank_Readings__c> tankReadingsList = new List<IMC_Chemical_Tank_Readings__c>();
        for(Integer i=1; i<=recNo; i++)
        {
            IMC_Chemical_Tank_Readings__c tsr = new IMC_Chemical_Tank_Readings__c();
        	tsr.IMC_Barcode__c='CIC085694';
            tankReadingsList.add(tsr);
        }
        
        return tankReadingsList;
    }
    
    //Update tank readings records for updated barcode value, it will be used to test bulk update tank readings records
    static void updateTankReadingRecords(List<IMC_Chemical_Tank_Readings__c> tankReadingsList, String newBarcode)
    {
        for(IMC_Chemical_Tank_Readings__c tankRead: tankReadingsList)
        {            
        	tankRead.IMC_Barcode__c=newBarcode;           
        }
        
    }
    
    // Verify assert the tank readings records has been created/update with new barcode and auto-populated the reference fields
    static void verifyAsserts()
    {
        List<IMC_Chemical_Tank_Readings__c> tankReadingQueryList= [SELECT Name, IMC_Barcode__c, IMC_Tank_ID__c,IMC_Reading_Unit__c, IMC_Tank_Name__c, IMC_Tank_Status__c 
                                                                       FROM IMC_Chemical_Tank_Readings__c ]; 
        for(IMC_Chemical_Tank_Readings__c tsr: tankReadingQueryList)
        {
            system.assert(tsr.IMC_Tank_Name__c != null, 'Expect Tank Info lookups is populated');	
            system.assert(tsr.IMC_Tank_ID__c !=null, 'Expect Tank Id is populated from Tank Info');
            system.assert(tsr.IMC_Reading_Unit__c != null , 'Expect Readigs unit is populated from tank Info');
        }
        
    }
    
    @isTest
    //creation of tank strap readings data when Barcode is not empty, this test is covering bulk insert/update as well
    static void tsrWithBarcode(){
        createTankInfo();        
        List<IMC_Chemical_Tank_Readings__c> tankReadingsList = createTankReadingRecords(TANK_READINGS_RECORD_COUNT);
       
        test.startTest();
            insert tankReadingsList;   
             //test for bulk insert for tank readings records
        	verifyAsserts();
            
            //test for bulk update for tank readings records
            updateTankReadingRecords(tankReadingsList, 'CIC085695');
            verifyAsserts();
        test.stopTest();   
        
    }    
    @isTest
    //Creation of record with Barcode is empty
    static void tsrWithNoBarcode(){
        createTankInfo();        
        IMC_Chemical_Tank_Readings__c tankReading = new IMC_Chemical_Tank_Readings__c();
        tankReading.IMC_Barcode__c='';       
        test.startTest();
        try{
            insert tankReading;  
        }                
        catch(Exception e){
            system.assert(e.getMessage().contains('Please enter/scan the valid Barcode'));
        }
        test.stopTest();        
    }
    @isTest
    //Creation of Tank Strap Reading record if the Barcode doesn't match any Tank Info record
    static void tsrWithNoBarcodeinTankInfo(){
        IMC_Chemical_Tank_Readings__c tankReading = new IMC_Chemical_Tank_Readings__c();
        tankReading.IMC_Barcode__c='CIC085694';       
        test.startTest();
        try{
            insert tankReading;  
        }
        catch(Exception e){
            system.assert(e.getMessage().contains('Barcode ' + tankReading.IMC_Barcode__c + ' is not found in Tank Info'));
        }
        test.stopTest();        
    }
   //Update barcode for existing tank readings record
    @isTest
    static void tsrWithBarcodeUpdate(){
        createTankInfo();        
        IMC_Chemical_Tank_Readings__c tsr = new IMC_Chemical_Tank_Readings__c();
        tsr.IMC_Barcode__c='CIC085694';       
        test.startTest();
            insert tsr;        
             
            tsr.IMC_Barcode__c='CIC085695';        
            update tsr;
        
        test.stopTest();      
        
        IMC_Chemical_Tank_Readings__c updateTankReadings= [SELECT Name, IMC_Barcode__c, IMC_Tank_ID__c,IMC_Reading_Unit__c, IMC_Tank_Name__c, IMC_Tank_Status__c
                                                       FROM IMC_Chemical_Tank_Readings__c 
                                                       WHERE IMC_Barcode__c = 'CIC085695' limit 1];                        
        
        system.assert(updateTankReadings.IMC_Tank_Name__c != null, 'Expect Tank Info lookups is updated based on updated barcode');	
        system.assert(updateTankReadings.IMC_Tank_ID__c =='16-1900', 'Expect Tank Id is populated for updated barcode');
        system.assert(updateTankReadings.IMC_Reading_Unit__c == 'PERCENT' , 'Expect Readigs unit is populated for updated barcode');
    }
    //Create tank reading with valid barcode, and then update it with invalid barcode 
    @isTest
    static void tsrWithDifferentBarcode(){
        createTankInfo();        
        IMC_Chemical_Tank_Readings__c tsr = new IMC_Chemical_Tank_Readings__c();
        tsr.IMC_Barcode__c='CIC085694';       
        test.startTest();
        insert tsr;        
        IMC_Chemical_Tank_Readings__c updateTankReadings = [SELECT IMC_Barcode__c 
                                                      FROM IMC_Chemical_Tank_Readings__c 
                                                      WHERE IMC_Barcode__c='CIC085694' limit 1];            
        updateTankReadings.IMC_Barcode__c='CIC085567';  
        try{
            update updateTankReadings;
        }
        catch(Exception e){
            system.assert(e.getMessage().contains('Barcode ' + updateTankReadings.IMC_Barcode__c + ' is not found in Tank Info'), 'Expect error message for barcode does not available in tank info');           
        }       
        test.stopTest();
    }
   //Creation of tank readings record without barcode
    @isTest
    static void tsrWithNoBarcodeUpdate(){
        createTankInfo();
        IMC_Chemical_Tank_Readings__c tsr = new IMC_Chemical_Tank_Readings__c();
        tsr.IMC_Barcode__c='CIC085694';
        test.startTest();
            insert tsr;
            IMC_Chemical_Tank_Readings__c updateTankReadings = [SELECT IMC_Tank_ID__c 
                                                          FROM IMC_Chemical_Tank_Readings__c 
                                                          WHERE IMC_Barcode__c='CIC085694' limit 1];
           
            updateTankReadings.IMC_Barcode__c='';          
            try{
                update updateTankReadings;
            }
            catch(Exception e){
                system.assert(e.getMessage().contains('Please enter/scan the valid Barcode'), 'Expect an error message for barcode is not entered');           
            }
        test.stopTest();       
    }   

}